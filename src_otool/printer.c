/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printer.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 15:47:14 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/03 15:15:19 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "otool.h"

static void		print_hex(t_file file, t_section sect, uint64_t pos)
{
	uint8_t		i;

	i = 0;
	while (i < 16 && pos + i < sect.size)
	{
		ft_printf("%02hhx",
			(char)*((char*)file.ptr + sect.offset + pos + i));
		if (file.cputype != CPU_TYPE_POWERPC || !((i + 1) % 4))
			ft_putchar(' ');
		++i;
	}
}

static int		print_section(t_file file, t_section sect, const char *title,
				t_bool is64)
{
	uint64_t	pos;
	uint64_t	position;

	pos = 0;
	ft_putendl(title);
	while (pos < sect.size)
	{
		if (sfcheck(file, file.ptr + sect.offset + pos + 16))
			return (EXIT_FAILURE);
		if (is64)
			ft_printf("%016llx\t", sect.addr + pos);
		else
			ft_printf("%08llx\t", sect.addr + pos);
		print_hex(file, sect, pos);
		ft_putchar('\n');
		pos += 16;
	}
	return (EXIT_SUCCESS);
}

int				print_sections(t_file file, t_sect sections, t_bmask opt)
{
	int		ret;

	ret = EXIT_SUCCESS;
	if (!(opt & OPT_T))
		ret = print_section(file, sections.text,
		"Contents of (__TEXT,__text) section", sections.is64);
	if (opt & OPT_D && sections.data.size)
		ret = print_section(file, sections.data,
			"Contents of (__DATA,__data) section", sections.is64);
	return (ret);
}

int				err_print_wrap(const char *fname, const char *err)
{
	ft_putstr(fname);
	return (ft_err_print(err));
}
