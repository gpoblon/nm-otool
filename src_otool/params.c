/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   params.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 17:11:24 by gpoblon           #+#    #+#             */
/*   Updated: 2019/04/26 16:11:40 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "otool.h"

int				get_file(const char *fname, t_file *file)
{
	int			fd;
	struct stat buf;

	ft_bzero(file, sizeof(t_file));
	file->name = fname;
	if ((fd = open(fname, O_RDONLY)) < 0)
		return (err_print_wrap(fname, ": No such file or directory."));
	if (fstat(fd, &buf) < 0)
		return (err_print_wrap(fname, ": Is not a valid object file"));
	if (S_ISDIR(buf.st_mode))
		return (err_print_wrap(fname, ": Is a directory"));
	if ((file->mapped_ptr =
		mmap(NULL, buf.st_size, PROT_READ, MAP_PRIVATE, fd, 0)) == MAP_FAILED)
		return (err_print_wrap(fname, ": Error: main::mmap() failed"));
	file->size = buf.st_size;
	file->ptr = file->mapped_ptr;
	file->mapped_size = buf.st_size;
	return (EXIT_SUCCESS);
}

static void		save_option(char opt_char, t_bmask *options)
{
	if (opt_char == 'd')
		*options |= OPT_D;
	else if (opt_char == 't')
		*options |= OPT_T;
	else
		*options |= OPT_ERR;
}

t_bmask			get_options(const char **av, unsigned int ac)
{
	unsigned int	i;
	t_bmask			options;
	unsigned int	opt_count_tmp;

	i = 0;
	options = 0;
	while (++i < ac && ft_strlen(av[i]) > 1)
	{
		if (!ft_strcmp(av[i], "--"))
			break ;
		if (av[i][0] == '-')
		{
			opt_count_tmp = 0;
			while (av[i][++opt_count_tmp])
			{
				save_option(av[i][opt_count_tmp], &options);
				if (options & OPT_ERR)
					return (options);
			}
		}
	}
	return (options);
}

unsigned int	get_argfile_count(const char **av, unsigned int ac)
{
	unsigned int	i;
	unsigned int	argfile_count;
	t_bool			is_file;

	i = 0;
	is_file = FALSE;
	argfile_count = 0;
	while (++i < ac)
	{
		if (!ft_strcmp(av[i], "--"))
			is_file = 1;
		else if (!ft_strcmp(av[i], "-"))
			continue ;
		else if (av[i][0] != '-' || is_file)
			++argfile_count;
	}
	return (argfile_count);
}
