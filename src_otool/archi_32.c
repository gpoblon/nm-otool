/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   archi_32.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 17:23:07 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/06 11:45:58 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "otool.h"

static void		sections_filler(t_file file, struct segment_command *seg,
				t_sect *sections)
{
	struct section		*sect;
	struct section		s;
	unsigned int		i;

	i = 0;
	sect = (struct section *)((char*)seg + sizeof(*seg));
	while (i < (file.issw ? swap32(seg->nsects) : seg->nsects))
	{
		s = sect[i];
		if (!ft_strncmp(SECT_TEXT, s.sectname, 16) &&
			!ft_strncmp(SEG_TEXT, s.segname, 16))
		{
			sections->text.size = (file.issw ? swap32(s.size) : s.size);
			sections->text.offset = (file.issw ? swap32(s.offset) : s.offset);
			sections->text.addr = (file.issw ? swap32(s.addr) : s.addr);
		}
		else if (!ft_strncmp(SECT_DATA, s.sectname, 16) &&
			!ft_strncmp(SEG_DATA, s.segname, 16))
		{
			sections->data.size = (file.issw ? swap32(s.size) : s.size);
			sections->data.offset = (file.issw ? swap32(s.offset) : s.offset);
			sections->data.addr = (file.issw ? swap32(s.addr) : s.addr);
		}
		++i;
	}
}

static void		handle_cmds(t_file file, struct load_command *lc,
				t_sect *sections)
{
	uint32_t	cmd;
	uint32_t	nsects;

	cmd = (file.issw ? swap32(lc->cmd) : lc->cmd);
	nsects = ((struct segment_command *)lc)->nsects;
	nsects = (file.issw ? swap32(nsects) : nsects);
	if (cmd == LC_SEGMENT && nsects > 0)
		sections_filler(file, (struct segment_command *)lc, sections);
}

int				mh_32(t_file file, t_sect *sections)
{
	struct load_command			*lc;
	uint32_t					i;
	uint32_t					ncmds;
	uint32_t					size;

	i = 0;
	sections->is64 = FALSE;
	if (sfcheck(file, file.ptr +
		(size = sizeof(struct mach_header)) + sizeof(struct load_command)))
		return (EXIT_FAILURE);
	lc = (struct load_command*)(file.ptr + sizeof(struct mach_header));
	ncmds = ((struct mach_header *)file.ptr)->ncmds;
	ncmds = (file.issw ? swap32(ncmds) : ncmds);
	while (i++ < ncmds)
	{
		size += (file.issw ? swap32(lc->cmdsize) : lc->cmdsize);
		if (sfcheck(file, file.ptr + size))
			return (EXIT_FAILURE);
		handle_cmds(file, lc, sections);
		lc = (void *)lc + (file.issw ? swap32(lc->cmdsize) : lc->cmdsize);
	}
	return (EXIT_SUCCESS);
}
