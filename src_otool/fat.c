/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fat.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/16 12:12:25 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/03 17:09:34 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "otool.h"

static int			is_arch_computer(t_file file, uint32_t arch_count,
					uint32_t *i)
{
	cpu_type_t	cputype;

	while (*i < arch_count)
	{
		cputype = ((struct fat_arch *)(file.mapped_ptr +
			sizeof(struct fat_header) + sizeof(struct fat_arch) * *i))->cputype;
		cputype = (file.issw ? swap32(cputype) : cputype);
		if (__x86_64 && cputype == CPU_TYPE_X86_64)
			return (TRUE);
		++(*i);
	}
	*i = 0;
	return (FALSE);
}

static void			get_fat_arch_meta_32(t_file file,
				struct fat_arch *fat_arch, uint32_t i)
{
	*fat_arch = *(struct fat_arch *)(file.mapped_ptr +
				sizeof(struct fat_header) + sizeof(struct fat_arch) * i);
	if (file.issw)
	{
		fat_arch->size = swap32(fat_arch->size);
		fat_arch->offset = swap32(fat_arch->offset);
		fat_arch->cputype = swap32(fat_arch->cputype);
	}
}

int					fat_32(t_file *file, t_sect *sections, t_bmask options)
{
	uint32_t		i;
	uint32_t		arch_to_read;
	struct fat_arch	fatarch;

	i = 0;
	file->ismultiple = TRUE;
	sections->is64 = FALSE;
	if (sfcheck(*file, file->ptr + sizeof(struct fat_header) + sizeof(fatarch)))
		return (EXIT_FAILURE);
	arch_to_read = (*(struct fat_header*)file->mapped_ptr).nfat_arch;
	arch_to_read = (file->issw ? swap32(arch_to_read) : arch_to_read);
	if (is_arch_computer(*file, arch_to_read, &i))
		arch_to_read = i + 1;
	if (!file->is_archi_single)
		file->is_archi_single = (arch_to_read - i == 1) ? TRUE : FALSE;
	while (i < arch_to_read)
	{
		get_fat_arch_meta_32(*file, &fatarch, i++);
		file->ptr = file->mapped_ptr + fatarch.offset;
		file->size = fatarch.size;
		if (file->ptr == file->mapped_ptr || sfcheck(*file, file->ptr +
			fatarch.size) || otool_from_fat(file, fatarch.cputype, options))
			return (EXIT_FAILURE);
	}
	return (EXIT_SUCCESS);
}

static void			get_fat_arch_meta_64(t_file file,
				struct fat_arch_64 *fat_arch, uint32_t i)
{
	*fat_arch = *(struct fat_arch_64 *)(file.mapped_ptr +
				sizeof(struct fat_header) + sizeof(struct fat_arch) * i);
	if (file.issw)
	{
		fat_arch->size = swap64(fat_arch->size);
		fat_arch->offset = swap64(fat_arch->offset);
		fat_arch->cputype = swap32(fat_arch->cputype);
	}
}

int					fat_64(t_file *file, t_sect *sections, t_bmask options)
{
	uint32_t			i;
	uint32_t			arch_to_read;
	struct fat_arch_64	fatarch;

	i = 0;
	file->ismultiple = TRUE;
	sections->is64 = TRUE;
	if (sfcheck(*file, file->ptr + sizeof(struct fat_header) + sizeof(fatarch)))
		return (EXIT_FAILURE);
	arch_to_read = (*(struct fat_header*)file->mapped_ptr).nfat_arch;
	arch_to_read = (file->issw ? swap32(arch_to_read) : arch_to_read);
	if (is_arch_computer(*file, arch_to_read, &i))
		arch_to_read = i + 1;
	if (!file->is_archi_single)
		file->is_archi_single = (arch_to_read - i == 1) ? TRUE : FALSE;
	while (i < arch_to_read)
	{
		get_fat_arch_meta_64(*file, &fatarch, i++);
		file->ptr = file->mapped_ptr + fatarch.offset;
		file->size = fatarch.size;
		if (file->ptr == file->mapped_ptr || sfcheck(*file, file->ptr +
			fatarch.size) || otool_from_fat(file, fatarch.cputype, options))
			return (EXIT_FAILURE);
	}
	return (EXIT_SUCCESS);
}
