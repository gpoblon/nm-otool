/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   otool.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/17 20:05:34 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/06 12:00:27 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "otool.h"

static int	handle_from_magic_number(t_file *file, t_sect *sections,
			t_bmask options)
{
	uint32_t	magic_number;

	if (sfcheck(*file, file->ptr + sizeof(magic_number)))
		return (EXIT_FAILURE);
	ft_bzero(sections, sizeof(t_sect));
	magic_number = *(int *)file->ptr;
	file->issw = (magic_number == MH_CIGAM_64 |
		magic_number == MH_CIGAM |
		magic_number == FAT_CIGAM |
		magic_number == FAT_CIGAM_64);
	if (magic_number == MH_MAGIC_64 || magic_number == MH_CIGAM_64)
		return (mh_64(*file, sections));
	else if (magic_number == MH_MAGIC || magic_number == MH_CIGAM)
		return (mh_32(*file, sections));
	else if (magic_number == FAT_CIGAM_64 || magic_number == FAT_MAGIC_64)
		return (fat_64(file, sections, options));
	else if (magic_number == FAT_CIGAM || magic_number == FAT_MAGIC)
		return (fat_32(file, sections, options));
	else if (!ft_strncmp((const char *)file->ptr, ARMAG, SARMAG))
	{
		ft_printf("Archive : %s\n", file->name);
		return (static_lib(file, options));
	}
	return (EXIT_FAILURE);
}

int			otool_from_fat(t_file *file, cpu_type_t archi, t_bmask options)
{
	t_sect		sections;
	uint32_t	is_fat_swapped;
	const char	*arch_str;

	is_fat_swapped = file->issw;
	arch_str = get_archi_str(archi);
	if (handle_from_magic_number(file, &sections, options))
		return (EXIT_FAILURE);
	else if (file->is_archi_single == FALSE && sections.text.size)
		ft_printf("%s (architecture %s):\n", file->name, arch_str);
	else if (!file->islib)
		ft_printf("%s:\n", file->name);
	file->cputype = archi;
	if (!file->islib)
		if (print_sections(*file, sections, options))
			return (EXIT_FAILURE);
	file->issw = is_fat_swapped;
	return (EXIT_SUCCESS);
}

int			otool_from_lib(t_file *file, const char *objname, t_bmask options)
{
	t_sect		sections;
	uint32_t	is_fat_swapped;

	file->islib = TRUE;
	is_fat_swapped = file->issw;
	if (handle_from_magic_number(file, &sections, options))
		return (EXIT_FAILURE);
	else if (sections.text.offset)
	{
		ft_printf("%s(%s):\n", file->name, objname);
		if (print_sections(*file, sections, options))
			return (EXIT_FAILURE);
	}
	file->issw = is_fat_swapped;
	return (EXIT_SUCCESS);
}

int			otool(const char *filename, t_bmask options)
{
	t_sect		sections;
	t_file		file;

	if (get_file(filename, &file) || file.mapped_size <= 0)
		return (EXIT_FAILURE);
	if (handle_from_magic_number(&file, &sections, options))
	{
		return (err_print_wrap(filename,
		": The file was not recognized as a valid object file"));
	}
	if (!file.ismultiple)
	{
		ft_putstr(file.name);
		ft_putstr(":\n");
		if (print_sections(file, sections, options))
			return (EXIT_FAILURE);
	}
	if (munmap(file.mapped_ptr, file.mapped_size) < 0)
		return (ft_err_print("Error: main::munmap() failed"));
	return (EXIT_SUCCESS);
}
