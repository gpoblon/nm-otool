/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libraries.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/16 12:12:25 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/03 17:33:05 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "otool.h"

static char	*strcheck_ar_name(char *name, int len)
{
	if (strcheck(len, name))
		return (name);
	else
		return (NULL);
}

static int	get_arch_pos(t_file *file)
{
	struct ar_hdr	arch;
	uint64_t		pos;

	pos = SARMAG;
	arch = *(struct ar_hdr *)(file->ptr + pos);
	pos += ft_atoin(arch.ar_size, sizeof(arch.ar_size)) + sizeof(arch);
	if (sfcheck(*file, file->ptr + pos))
		return (EXIT_FAILURE);
	file->ptr += pos;
	return (EXIT_SUCCESS);
}

static int	current_arch_lib(t_file *file, t_bmask options)
{
	struct ar_hdr	arch;
	void			*name_addr;
	char			*ar_name;

	arch = *(struct ar_hdr *)file->ptr;
	name_addr = file->ptr + sizeof(struct ar_hdr);
	file->ptr = name_addr + ft_atoi(arch.ar_name + 3);
	file->size = ft_atoin(arch.ar_size, sizeof(arch.ar_size)) -
		ft_atoi(arch.ar_name + 3);
	ar_name = ft_strndup(name_addr, ft_atoi(arch.ar_name + 3));
	if (sfcheck(*file, file->ptr + file->size) ||
		otool_from_lib(file, ar_name, options))
	{
		free(ar_name);
		return (EXIT_FAILURE);
	}
	free(ar_name);
	file->ptr += file->size;
	return (EXIT_SUCCESS);
}

int			static_lib(t_file *file, t_bmask options)
{
	void			*end_of_arch;

	file->ismultiple = TRUE;
	end_of_arch = file->ptr + file->size;
	if (sfcheck(*file, file->ptr + sizeof(struct ar_hdr)) || get_arch_pos(file))
		return (EXIT_FAILURE);
	while (file->ptr + sizeof(struct ar_hdr) < end_of_arch)
		if (current_arch_lib(file, options))
			return (EXIT_FAILURE);
	return (EXIT_SUCCESS);
}
