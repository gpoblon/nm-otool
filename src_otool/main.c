/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 12:41:54 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/06 12:00:51 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "otool.h"

static int	otool_on_argfiles(const char **av, unsigned int ac, t_bmask opt)
{
	unsigned int	i;
	t_bool			is_file;

	i = 0;
	is_file = FALSE;
	while (++i < ac)
	{
		if (!ft_strcmp(av[i], "--"))
			is_file = 1;
		else if (!ft_strcmp(av[i], "-"))
			continue ;
		if (av[i][0] != '-' || is_file)
			if (otool(av[i], opt))
				return (EXIT_FAILURE);
	}
	return (EXIT_SUCCESS);
}

int			main(int ac, char **av)
{
	unsigned int	i;
	t_bmask			options;
	unsigned int	argfile_count;

	i = 0;
	argfile_count = 0;
	options = get_options((const char **)av, ac);
	argfile_count = get_argfile_count((const char **)av, ac);
	if (options & OPT_ERR)
		return (ft_err_print("Options is invalid\n\
		Usage: ./ft_otool -d [file.obj file.a file.so file.dylib...]"));
	if (argfile_count < 1)
		return (ft_err_print("At least one file must be specified"));
	if (otool_on_argfiles((const char **)av, ac, options))
		return (EXIT_FAILURE);
	return (EXIT_SUCCESS);
}
