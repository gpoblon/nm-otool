/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   otool.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/12 13:48:07 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/06 12:00:34 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OTOOL_H
# define OTOOL_H

# include "libft.h"

# include <fcntl.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/stat.h>
# include <sys/mman.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <mach-o/fat.h>
# include <ar.h>

# define OPT_T		1 << 2
# define OPT_D		1 << 1
# define OPT_ERR	1 << 0

typedef struct	s_section_meta
{
	uint64_t	offset;
	uint64_t	size;
	uint64_t	addr;
}				t_section;

typedef struct	s_section_dump
{
	t_section	data;
	t_section	text;
	t_bool		is64;
	t_bool		issw;
}				t_sect;

typedef struct	s_file_meta
{
	void		*ptr;
	void		*mapped_ptr;
	uint64_t	size;
	uint64_t	mapped_size;
	const char	*name;
	cpu_type_t	cputype;
	t_bool		issw;
	t_bool		ismultiple;
	t_bool		islib;
	t_bool		is_archi_single;
}				t_file;

/*
** params
*/
int				get_file(const char *fname, t_file *file);
t_bmask			get_options(const char **av, unsigned int ac);
unsigned int	get_argfile_count(const char **av, unsigned int ac);

/*
** otool
*/
int				otool_from_fat(t_file *file, cpu_type_t archi, t_bmask opt);
int				otool_from_lib(t_file *file, const char *objname, t_bmask opt);
int				otool(const char *filename, t_bmask opt);

/*
** archi
*/
int				mh_32(t_file file, t_sect *sections);
int				mh_64(t_file file, t_sect *sections);
int				fat_32(t_file *file, t_sect *sections, t_bmask options);
int				fat_64(t_file *file, t_sect *sections, t_bmask options);
int				static_lib(t_file *file, t_bmask options);

/*
** printer
*/
int				err_print_wrap(const char *fname, const char *err);
int				print_sections(t_file file, t_sect sections, t_bmask opt);

/*
** utils
*/
const char		*get_archi_str(cpu_type_t cputype);
uint64_t		swap64(uint64_t e);
uint32_t		swap32(uint32_t e);
int				sfcheck(t_file file, void *tocheck);
int				strcheck(uint64_t size, const char *str);

#endif
