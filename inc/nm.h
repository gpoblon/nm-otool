/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/12 13:48:07 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/06 11:59:28 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NM_H
# define NM_H

# include "libft.h"

# include <fcntl.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/stat.h>
# include <sys/mman.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <mach-o/fat.h>
# include <ar.h>

# define OPT_J		1 << 6
# define OPT_WU		1 << 5
# define OPT_U		1 << 4
# define OPT_R		1 << 3
# define OPT_N		1 << 2
# define OPT_G		1 << 1
# define OPT_ERR	1 << 0

typedef struct	s_symbol_data
{
	uint64_t	value;
	char		*name;
	char		ctype;
}				t_symdat;

typedef struct	s_section_meta
{
	uint32_t	count;
	uint8_t		data;
	uint8_t		text;
	uint8_t		bss;
}				t_section;

typedef struct	s_symtab_data
{
	t_symdat	*symtab;
	t_section	sect;
	uint32_t	len;
	uint32_t	stroff;
	t_bool		is64;
}				t_symtab;

typedef struct	s_file_meta
{
	void		*ptr;
	void		*mapped_ptr;
	uint64_t	size;
	uint64_t	mapped_size;
	t_bool		issw;
	t_bool		isfat;
	t_bool		is_archi_single;
	const char	*name;
}				t_file;

/*
** params
*/
t_bmask			get_options(const char **av, unsigned int ac);
unsigned int	get_argfile_count(const char **av, unsigned int ac);
int				get_file(const char *fname, t_file *file);

/*
** nm
*/
int				nm(const char *filename, t_bool is_unique, t_bmask opt);
int				nm_from_lib(t_file *file, const char *objname, t_bmask opt);
int				nm_from_fat(t_file *file, cpu_type_t archi, t_bmask opt);

/*
** architecture parser (archi_32, 64, fat)
*/
int				mh_64(t_file file, t_symtab *symtab);
int				mh_32(t_file file, t_symtab *symtab);
int				fat_32(t_file *file, t_symtab *symtab, t_bmask options);
int				fat_64(t_file *file, t_symtab *symtab, t_bmask options);
int				static_lib(t_file *file, t_symtab *symtab, t_bmask options);

/*
** printer
*/
void			print_sorted_symtab(t_symtab symtab,
				t_bmask opt);
int				err_print_wrap(const char *fname, const char *err);

/*
** sort
*/
void			apply_options_sort(t_symtab *symtab, t_bmask options);

/*
** get_types
*/
char			get_symtype_char(t_symdat sym, t_symtab symtab, uint8_t type,
				uint8_t sect);

/*
** utils
*/
const char		*get_archi_str(cpu_type_t cputype);
uint64_t		swap64(uint64_t e);
uint32_t		swap32(uint32_t e);
int				sfcheck(t_file file, void *tocheck);
int				strcheck(uint64_t size, const char *str);

#endif
