# NM command repro: display name list (symbol table)

## man nm
Nm displays the name list (symbol table) of each object file in the argument list.
If an argument is an archive, a listing for each object file in the archive will be produced. File can be of the form libx.a(x.o), in which case only symbols from that member of the object file are listed. (The parentheses have to be quoted to get by the shell.)
If no file is given,the symbols in a.out are listed.

Output is made of addresses, types, and symbol names: each symbol name is preceded by its value (blanks if undefined). This value is followed by one of the following characters, representing the symbol type:
U (undefined) means the symbol is referenced in this object but its value was defined elsewhere,
A (absolute),
T (text section symbol),
D (data section symbol),
B (bss section symbol),
C (common symbol),
\- (for debugger symbol table entries; see -a below),
S (symbol in a section other than those above), or
I (indirect symbol).
If the symbol is local (non-external), the symbol's type is instead represented by the corresponding lowercase letter.
A lower case u in a dynamic shared library indicates an undefined reference to a private external in another module in the same library.

If the symbol is a Objective C method, the symbol name is +-[Class_name(category_name) method:name:], where `+' is for class methods, `-' is for instance methods, and (category_name) is present only when the method is in a category.

The output is sorted alphabetically by default.

`nm` command outputs 3 columns representing the symbol value, symbol type, and symbol name,respectively.

## Infos web
Table des symboles est une structure de données: metadata sur les adresses des variables et fonctions.
The minimum information contained in a symbol table used by a translator includes the symbol's name, its relocatability attributes (absolute, relocatable, etc.), and its location or address. For relocatable symbols, some relocation information must be stored. 

## Fichiers geres
Macho 64/32
archives
fat

## MACH_O FORMAT

use these files to understand how it works and every offset / variable / symbols positions: */usr/include/mach-o/\**. Note that I am only using *loader.h* and *n_list.h* as of now (ie: `nm` repor without any options, dealing only with basic *object files*)

The MACHO header is at the very beginning f the file (offset = 0)
```
struct mach_header_64 { // || mach_header
    uint32_t    magic;      /* mach magic number identifier */
    cpu_type_t  cputype;    /* cpu specifier */
    cpu_subtype_t   cpusubtype; /* machine specifier */
    uint32_t    filetype;   /* type of file */
    uint32_t    ncmds;      /* number of load commands */
    uint32_t    sizeofcmds; /* the size of all the load commands */
    uint32_t    flags;      /* flags */
    uint32_t    reserved;   /* reserved */
};
```
It is followed by a list of `mach_header->ncmds` `load_commands` : 
```
struct load_command {
    uint32_t cmd;       /* type of load command */
    uint32_t cmdsize;   /* total size of command in bytes */
};
```
There are several types of `load_command`s such as one+ `segment_command`s or the `symtab_command` (`LC_THREAD` (`thread_command`), `LC_ROUTINES LC_PREPARE LC_DYSYMTAB` etc... depending on the file). The `symtab_command` (address `LC_SYMTAB`) being the one containing what is interesting for us, ie: the list of the symbols (*symbol table*) printed by the NIX `nm` command
There seems to be only one `symtab_command` by macho file
```
struct symtab_command {
    uint32_t    cmd;        /* LC_SYMTAB */
    uint32_t    cmdsize;    /* sizeof(struct symtab_command) */
    uint32_t    symoff;     /* symbol table offset */
    uint32_t    nsyms;      /* number of symbol table entries */
    uint32_t    stroff;     /* string table offset */
    uint32_t    strsize;    /* string table size in bytes */
};
```
Each symbol has the following structure (64/32):
```
struct nlist_64 { // || nlist
    union {
        uint32_t  n_strx; /* index into the string table */ /* STRINGTABLE OFFSET OF THE STRING NAME TO PRINT */
    } n_un;
    uint8_t n_type;        /* type flag, see below */ /* CONTAINS THE TYPE TO PRINT ONCE PARSED */
    uint8_t n_sect;        /* section number or NO_SECT */
    uint16_t n_desc;       /* see <mach-o/stab.h> */
    uint64_t n_value;      /* value of this symbol (or stab offset) || uint32*/ /* CONTAINS THE VALUE TO DIRECTLY PRINT AS A %016llx in printf */
};
```
Use this to find out what the type is from the `unsigned short n_type`. From the *n_list.h* file
Note that: if lowercase, the symbol is usually local; if uppercase, the symbol is global (external). There are however a few lowercase symbols that are shown for special global symbols (u, v and w).
```
/*
 * The n_type field really contains four fields:
 *	unsigned char N_STAB:3,
 *		      N_PEXT:1,
 *		      N_TYPE:3,
 *		      N_EXT:1;
 * which are used via the following masks.
 */
#define	N_STAB	0xe0  /* if any of these bits set, a symbolic debugging entry */
#define	N_PEXT	0x10  /* private external symbol bit */
#define	N_TYPE	0x0e  /* mask for the type bits */
#define	N_EXT	0x01  /* external symbol bit, set for external symbols */

/*
 * Only symbolic debugging entries have some of the N_STAB bits set and if any
 * of these bits are set then it is a symbolic debugging entry (a stab).  In
 * which case then the values of the n_type field (the entire field) are given
 * in <mach-o/stab.h>
 */

/*
 * Values for N_TYPE bits of the n_type field.
 */
#define	N_UNDF	0x0		/* undefined, n_sect == NO_SECT */
#define	N_ABS	0x2		/* absolute, n_sect == NO_SECT */
#define	N_SECT	0xe		/* defined in section number n_sect */
#define	N_PBUD	0xc		/* prebound undefined (defined in a dylib) */
#define N_INDR	0xa		/* indirect */
```

With all this you can guess the type of most symbols but `N_SECT` ones, that require external knowledge (from the `segment_command`)

Each symbol has a `n_sect` value. `N_SECT` symbols actually point to a particular section (sections are located in segment commands)
### Segment (`__TEXT`, `__DATA` etc) = one+ sections
```
struct segment_command_64 { /* for 64-bit architectures */
	uint32_t	cmd;		/* LC_SEGMENT_64 */
	uint32_t	cmdsize;	/* includes sizeof section_64 structs */
	char		segname[16];	/* segment name */
	uint64_t	vmaddr;		/* memory address of this segment */
	uint64_t	vmsize;		/* memory size of this segment */
	uint64_t	fileoff;	/* file offset of this segment */
	uint64_t	filesize;	/* amount to map from the file */
	vm_prot_t	maxprot;	/* maximum VM protection */
	vm_prot_t	initprot;	/* initial VM protection */
	uint32_t	nsects;		/* number of sections in segment */
	uint32_t	flags;		/* flags */
};
```
If the 64-bit segment has sections then section_64 structures directly follow a 64-bit segment command. Note that there can be several segments that contain sections.
```
struct section_64 { /* for 64-bit architectures */
	char		sectname[16];	/* name of this section */
	char		segname[16];	/* segment this section goes in */
	uint64_t	addr;		/* memory address of this section */
	uint64_t	size;		/* size in bytes of this section */
	uint32_t	offset;		/* file offset of this section */
	uint32_t	align;		/* section alignment (power of 2) */
	uint32_t	reloff;		/* file offset of relocation entries */
	uint32_t	nreloc;		/* number of relocation entries */
	uint32_t	flags;		/* flags (section type and attributes)*/
	uint32_t	reserved1;	/* reserved (for offset or index) */
	uint32_t	reserved2;	/* reserved (for count or sizeof) */
	uint32_t	reserved3;	/* reserved */
};
```

Example of the list of sections of an executable:
```
---- // first segment
__TEXT
__text, __TEXT (1)
__stubs, __TEXT (2...)
__stub_helper, __TEXT
__cstring, __TEXT
__const, __TEXT
__unwind_info, __TEXT
---- // new segment
__DATA
__got, __DATA
__nl_symbol_ptr, __DATA
__la_symbol_ptr, __DATA
__const, __DATA
__data, __DATA (11)
__common, __DATA (12)
__bss, __DATA (13)
```
This means if a `n_sect` has a value of `12` it will have the type `data`, if it has the value `1` it will have the type `text`

## gerer fat
```
struct fat_header {
	uint32_t	magic;		/* FAT_MAGIC or FAT_MAGIC_64 */
	uint32_t	nfat_arch;	/* number of structs that follow */
};

struct fat_arch {
	cpu_type_t	cputype;	/* cpu specifier (int) */
	cpu_subtype_t	cpusubtype;	/* machine specifier (int) */
	uint32_t	offset;		/* file offset to this object file */
	uint32_t	size;		/* size of this object file */
	uint32_t	align;		/* alignment as a power of 2 */
};
```

## gerer archives
start at : `offset = SARMAG + (struct ar_hdr).ar_size + sizeof(struct ar_hdr)`
then loop on: `offset += file->size + sizeof(struct ar_hdr)`
to bring your offset to new elements and nm it as it were files

## Best sorting algo
quick sort vs merge sort: quick sort works better with arrays of small size (< 10000 elements), does not require external memory
https://www.geeksforgeeks.org/quick-sort-vs-merge-sort/
https://softwareengineering.stackexchange.com/questions/146173/merge-sort-versus-quick-sort-performance
http://sorting.at/ (examples)


# OTOOL command repro: object file displaying tool

## man otool (-t)
otool [ option ... ] [ file ... ]

The otool command displays specified parts of object files or libraries.
If the -m option is not used the file arguments may be of the form libx.a(foo.o), to request information about only that object file and not the entire library. (Typically this argument must be quoted, ``libx.a(foo.o)'', to get it past the shell.)
Otool understands both Mach-O (Mach object) files and universal file formats.
Otool can display the specified information in its raw (umeric) form` (without the -v flag).

(default) `-t` option: Display the contents of the (__TEXT,__text) section.
