/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printer.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 15:47:14 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/06 10:31:47 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"

static void	print_sym_value(t_symdat sym, t_bool is64)
{
	if (sym.ctype != 'U' && sym.ctype != 'u')
	{
		if (is64)
			ft_printf("%016llx", sym.value);
		else
			ft_printf("%08llx", sym.value);
	}
	else
	{
		if (is64)
			ft_printf("%16c", ' ');
		else
			ft_printf("%8c", ' ');
	}
}

static void	print_sym(t_symdat sym, t_bool is64, t_bmask opt)
{
	if ((opt & OPT_G && !ft_isupper(sym.ctype)) || sym.ctype == '-' ||
		(opt & OPT_WU && (sym.ctype == 'U' || sym.ctype == 'u')))
		return ;
	if (opt & OPT_U)
	{
		if (!(opt & OPT_WU) && (sym.ctype == 'U' || sym.ctype == 'u'))
			ft_printf("%s\n", sym.name);
		return ;
	}
	if (opt & OPT_J)
	{
		ft_printf("%s\n", sym.name);
		return ;
	}
	print_sym_value(sym, is64);
	ft_printf(" %c %s\n", sym.ctype, sym.name);
}

void		print_sorted_symtab(t_symtab symtab, t_bmask opt)
{
	int64_t		i;
	t_symdat	sym;

	if ((opt & OPT_R) && (opt & OPT_N))
	{
		i = symtab.len;
		while (--i >= 0)
		{
			sym = symtab.symtab[i];
			print_sym(sym, symtab.is64, opt);
		}
	}
	else
	{
		i = 0;
		while (i < symtab.len)
		{
			sym = symtab.symtab[i++];
			print_sym(sym, symtab.is64, opt);
		}
	}
}

int			err_print_wrap(const char *fname, const char *err)
{
	ft_putstr(fname);
	return (ft_err_print(err));
}
