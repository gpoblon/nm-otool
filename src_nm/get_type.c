/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_type.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/15 14:15:37 by gpoblon           #+#    #+#             */
/*   Updated: 2019/04/30 15:55:09 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"

const char	*get_archi_str(cpu_type_t cputype)
{
	if (__x86_64 && cputype == CPU_TYPE_X86_64)
		return (NULL);
	else if (cputype == CPU_TYPE_I386)
		return ("i386");
	else if (cputype == CPU_TYPE_X86_64)
		return ("x86_64");
	else if (cputype == CPU_TYPE_POWERPC)
		return ("ppc");
	else
		return (NULL);
}

static char	get_symtype_char_sect(uint32_t sym_sect, t_symtab symtab)
{
	char	c;

	if (symtab.sect.text == sym_sect)
		c = 't';
	else if (symtab.sect.data == sym_sect)
		c = 'd';
	else if (symtab.sect.bss == sym_sect)
		c = 'b';
	else
		c = 's';
	return (c);
}

char		get_symtype_char(t_symdat sym, t_symtab symtab, uint8_t type,
			uint8_t sect)
{
	uint32_t	mask_type;
	char		c;

	mask_type = type & N_TYPE;
	if (type & N_STAB)
		c = '-';
	else
	{
		if ((mask_type == N_UNDF && sym.value == 0) || mask_type == N_PBUD)
			c = 'u';
		else if (mask_type == N_UNDF && sym.value != 0)
			c = 'c';
		else if (mask_type == N_SECT)
			c = get_symtype_char_sect(sect, symtab);
		else if (mask_type == N_INDR)
			c = 'i';
		else if (mask_type == N_ABS)
			c = 'a';
		else
			c = ' ';
	}
	if ((type & N_EXT) && c != '-')
		c = ft_toupper(c);
	return (c);
}
