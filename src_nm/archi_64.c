/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   archi_64.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/02 15:42:28 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/06 11:49:35 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"

static int		sym_filler_64(t_file file, struct nlist_64 sym, t_symtab symtab,
				t_symdat *symdat)
{
	void		*name_ptr;
	uint32_t	n_strx;

	n_strx = (file.issw ? swap32(sym.n_un.n_strx) : sym.n_un.n_strx);
	name_ptr = file.ptr + symtab.stroff + n_strx;
	if (!n_strx || sfcheck(file, file.ptr + symtab.stroff + n_strx) ||
		strcheck(file.size - (name_ptr - file.ptr), (const char *)name_ptr))
		return (EXIT_FAILURE);
	symdat->value = (file.issw ? swap64(sym.n_value) : sym.n_value);
	symdat->ctype = get_symtype_char(*symdat, symtab, sym.n_type, sym.n_sect);
	symdat->name = (char *)name_ptr;
	return (EXIT_SUCCESS);
}

static int		symtab_filler_64(t_file file, struct symtab_command sym_cmd,
				t_symtab *symtab)
{
	struct nlist_64	*fsymlst;
	uint32_t		i;
	uint32_t		symoff;
	uint32_t		strsize;

	i = 0;
	symoff = (file.issw ? swap32(sym_cmd.symoff) : sym_cmd.symoff);
	strsize = (file.issw ? swap32(sym_cmd.strsize) : sym_cmd.strsize);
	if (!(symtab->len = (file.issw ? swap32(sym_cmd.nsyms) : sym_cmd.nsyms)))
		return (EXIT_SUCCESS);
	symtab->stroff = (file.issw ? swap32(sym_cmd.stroff) : sym_cmd.stroff);
	if (sfcheck(file, file.ptr + symoff + (symtab->len * sizeof(*fsymlst))) ||
		sfcheck(file, file.ptr + symtab->stroff + strsize))
		return (EXIT_FAILURE);
	fsymlst = (struct nlist_64*)(file.ptr + symoff);
	symtab->symtab = (t_symdat*)malloc(sizeof(t_symdat) * symtab->len);
	while (i < symtab->len)
	{
		if (sym_filler_64(file, fsymlst[i], *symtab, &(symtab->symtab[i])))
			return (EXIT_FAILURE);
		++i;
	}
	return (EXIT_SUCCESS);
}

static void		sections_filler_64(t_file file, struct segment_command_64 *seg,
				t_symtab *symtab)
{
	struct section_64	*sect;
	uint32_t			nsects;
	unsigned int		i;

	i = 0;
	symtab->is64 = TRUE;
	nsects = (file.issw ? swap32(seg->nsects) : seg->nsects);
	sect = (struct section_64 *)((char*)seg + sizeof(*seg));
	while (i < nsects)
	{
		if (!ft_strncmp(SECT_TEXT, sect[i].sectname, 16))
			symtab->sect.text = symtab->sect.count;
		else if (!ft_strncmp(SECT_DATA, sect[i].sectname, 16))
			symtab->sect.data = symtab->sect.count;
		else if (!ft_strncmp(SECT_BSS, sect[i].sectname, 16))
			symtab->sect.bss = symtab->sect.count;
		++i;
		++(symtab->sect.count);
	}
}

static void		handle_cmds_64(t_file file, struct load_command *lc,
				struct symtab_command *sym_cmd, t_symtab *symtab)
{
	uint32_t	cmd;
	uint32_t	nsects;

	cmd = (file.issw ? swap32(lc->cmd) : lc->cmd);
	nsects = ((struct segment_command_64 *)lc)->nsects;
	nsects = (file.issw ? swap32(nsects) : nsects);
	if (cmd == LC_SEGMENT_64 && nsects > 0)
		sections_filler_64(file, (struct segment_command_64 *)lc, symtab);
	else if (cmd == LC_SYMTAB)
		*sym_cmd = *((struct symtab_command*)lc);
}

int				mh_64(t_file file, t_symtab *symtab)
{
	struct symtab_command		sym_cmd;
	struct load_command			*lc;
	uint32_t					i;
	uint32_t					ncmds;
	uint32_t					size;

	i = 0;
	symtab->sect.count = 1;
	sym_cmd.nsyms = 0;
	if (sfcheck(file, file.ptr +
		(size = sizeof(struct mach_header_64)) + sizeof(struct load_command)))
		return (EXIT_FAILURE);
	lc = (struct load_command*)(file.ptr + sizeof(struct mach_header_64));
	ncmds = ((struct mach_header_64 *)file.ptr)->ncmds;
	ncmds = (file.issw ? swap32(ncmds) : ncmds);
	while (i++ < ncmds)
	{
		size += (file.issw ? swap32(lc->cmdsize) : lc->cmdsize);
		if (sfcheck(file, file.ptr + size))
			return (EXIT_FAILURE);
		handle_cmds_64(file, lc, &sym_cmd, symtab);
		lc = (void *)lc + (file.issw ? swap32(lc->cmdsize) : lc->cmdsize);
	}
	return (symtab_filler_64(file, sym_cmd, symtab));
}
