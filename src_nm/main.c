/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 17:53:44 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/06 11:59:40 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"

static int	nm_on_argfiles(const char **av, unsigned int ac,
				unsigned int argfile_count, t_bmask opt)
{
	unsigned int	i;
	t_bool			is_file;

	i = 0;
	is_file = FALSE;
	if (!argfile_count)
	{
		if (nm((const char*)"a.out", TRUE, opt))
			return (EXIT_FAILURE);
	}
	else
	{
		while (++i < ac)
		{
			if (!ft_strcmp(av[i], "--"))
				is_file = 1;
			else if (!ft_strcmp(av[i], "-"))
				continue ;
			if (av[i][0] != '-' || is_file)
				if (nm(av[i], (argfile_count > 1 ? FALSE : TRUE), opt))
					return (EXIT_FAILURE);
		}
	}
	return (EXIT_SUCCESS);
}

int			main(int ac, char **av)
{
	unsigned int	i;
	t_bmask			options;
	unsigned int	argfile_count;

	i = 0;
	argfile_count = 0;
	options = get_options((const char **)av, ac);
	argfile_count = get_argfile_count((const char **)av, ac);
	if (options & OPT_ERR)
		return (ft_err_print("Options is invalid or occured multiple times\n\
		Usage: ./ft_nm -gnruUj [file.obj file.a file.so file.dylib...]"));
	if (nm_on_argfiles((const char **)av, ac, argfile_count, options))
		return (EXIT_FAILURE);
	return (EXIT_SUCCESS);
}
