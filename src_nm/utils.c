/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:57:36 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/02 17:54:03 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"

/*
** swap bytes for big endian
*/

uint64_t	swap64(uint64_t le)
{
	uint64_t	ge;
	int			i;
	char		*lec;
	char		*gec;

	lec = (char*)&le;
	gec = (char*)&ge;
	i = 0;
	while (i < 8)
	{
		gec[i] = lec[7 - i];
		++i;
	}
	return (ge);
}

uint32_t	swap32(uint32_t le)
{
	uint64_t	ge;
	int			i;
	char		*lec;
	char		*gec;

	lec = (char*)&le;
	gec = (char*)&ge;
	i = 0;
	while (i < 4)
	{
		gec[i] = lec[3 - i];
		++i;
	}
	return (ge);
}

/*
** segfault checker, to ensure the file size > to the address we want to reach
*/

int			sfcheck(t_file file, void *tocheck)
{
	void	*fsize;

	fsize = file.mapped_ptr + file.mapped_size;
	if ((uint64_t)fsize < (uint64_t)tocheck)
		return (EXIT_FAILURE);
	return (EXIT_SUCCESS);
}

int			strcheck(uint64_t size, const char *str)
{
	uint64_t	i;

	i = 0;
	while (i < size)
		if (str[i++] == '\0')
			return (EXIT_SUCCESS);
	return (EXIT_FAILURE);
}
