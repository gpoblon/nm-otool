/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/17 20:05:34 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/06 11:59:17 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"

static int	handle_from_magic_number(t_file *file, t_symtab *symtab,
			t_bmask options)
{
	uint32_t	magic_number;

	if (sfcheck(*file, file->ptr + sizeof(magic_number)))
		return (EXIT_FAILURE);
	ft_bzero(symtab, sizeof(t_symtab));
	magic_number = *(int *)file->ptr;
	file->issw = (magic_number == MH_CIGAM_64 |
		magic_number == MH_CIGAM |
		magic_number == FAT_CIGAM |
		magic_number == FAT_CIGAM_64);
	if (magic_number == MH_MAGIC_64 || magic_number == MH_CIGAM_64)
		return (mh_64(*file, symtab));
	else if (magic_number == MH_MAGIC || magic_number == MH_CIGAM)
		return (mh_32(*file, symtab));
	else if (magic_number == FAT_CIGAM_64 || magic_number == FAT_MAGIC_64)
		return (fat_64(file, symtab, options));
	else if (magic_number == FAT_CIGAM || magic_number == FAT_MAGIC)
		return (fat_32(file, symtab, options));
	else if (!ft_strncmp((const char *)file->ptr, ARMAG, SARMAG))
		return (static_lib(file, symtab, options));
	return (EXIT_FAILURE);
}

int			nm_from_fat(t_file *file, cpu_type_t archi, t_bmask options)
{
	t_symtab	symtab;
	uint32_t	is_fat_swapped;
	const char	*arch_str;

	is_fat_swapped = file->issw;
	arch_str = get_archi_str(archi);
	if (handle_from_magic_number(file, &symtab, options))
		return (EXIT_FAILURE);
	else if (file->is_archi_single == FALSE)
		ft_printf("\n%s (for architecture %s):\n", file->name, arch_str);
	else if (arch_str)
		ft_printf("%s:\n", file->name);
	apply_options_sort(&symtab, options);
	print_sorted_symtab(symtab, options);
	if (symtab.symtab)
		free(symtab.symtab);
	file->issw = is_fat_swapped;
	return (EXIT_SUCCESS);
}

int			nm_from_lib(t_file *file, const char *objname, t_bmask options)
{
	t_symtab	symtab;
	uint32_t	is_fat_swapped;

	is_fat_swapped = file->issw;
	if (handle_from_magic_number(file, &symtab, options))
		return (EXIT_FAILURE);
	else
		ft_printf("\n%s(%s):\n", file->name, objname);
	apply_options_sort(&symtab, options);
	print_sorted_symtab(symtab, options);
	if (symtab.symtab)
		free(symtab.symtab);
	file->issw = is_fat_swapped;
	return (EXIT_SUCCESS);
}

int			nm(const char *filename, t_bool is_unique, t_bmask options)
{
	t_symtab	symtab;
	t_file		file;

	if (get_file(filename, &file) || file.mapped_size <= 0)
		return (EXIT_FAILURE);
	if (handle_from_magic_number(&file, &symtab, options))
	{
		return (err_print_wrap(filename,
		": The file was not recognized as a valid object file"));
	}
	if (!file.isfat)
	{
		if (!is_unique)
			ft_printf("\n%s:\n", filename);
		apply_options_sort(&symtab, options);
		print_sorted_symtab(symtab, options);
		if (symtab.symtab)
			free(symtab.symtab);
	}
	if (munmap(file.mapped_ptr, file.mapped_size) < 0)
		return (ft_err_print("Error: main::munmap() failed"));
	return (EXIT_SUCCESS);
}
