/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gpoblon <gpoblon@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 15:53:34 by gpoblon           #+#    #+#             */
/*   Updated: 2019/05/06 11:48:31 by gpoblon          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"

static int	cmp_num(void *a, void *b)
{
	t_symdat		*ac;
	t_symdat		*bc;
	int				atype;
	int				btype;
	int				cmprep;

	ac = (t_symdat*)a;
	bc = (t_symdat*)b;
	atype = ft_isupper(ac->ctype) ? ac->ctype : ac->ctype - (32 + 26);
	btype = ft_isupper(bc->ctype) ? bc->ctype : bc->ctype - (32 + 26);
	if (ac->value > bc->value)
		return (1);
	else if (ac->value < bc->value)
		return (-1);
	else if (atype != btype)
		return (btype - atype);
	else
		return (ft_strcmp(ac->name, bc->name));
}

static int	cmp_str(void *a, void *b)
{
	int		cmpres;

	cmpres = ft_strcmp(((t_symdat*)a)->name, ((t_symdat*)b)->name);
	if (cmpres == 0)
		return (cmp_num(a, b));
	return (cmpres);
}

static int	cmp_rev(void *a, void *b)
{
	int		cmpres;

	cmpres = ft_strcmp(((t_symdat*)b)->name, ((t_symdat*)a)->name);
	if (cmpres == 0)
		return (cmp_num(b, a));
	return (cmpres);
}

void		apply_options_sort(t_symtab *symtab, t_bmask options)
{
	if (options & OPT_N)
		ft_qsort(symtab->symtab, sizeof(t_symdat), symtab->len, &cmp_num);
	else if (options & OPT_R)
		ft_qsort(symtab->symtab, sizeof(t_symdat), symtab->len, &cmp_rev);
	else
		ft_qsort(symtab->symtab, sizeof(t_symdat), symtab->len, &cmp_str);
}
