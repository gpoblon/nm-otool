#NM COMPILATION
NAME_NM		=	ft_nm
SRC_NM		= 	main.c \
				nm.c \
				params.c \
				archi_64.c \
				archi_32.c \
				fat.c \
				libraries.c \
				printer.c \
				sort.c \
				get_type.c \
				utils.c
SRC_D_NM	=	src_nm/
OBJ_D_NM	=	obj_nm/
OBJ_P_NM	=	$(addprefix $(OBJ_D_NM), $(SRC_NM:.c=.o))

#OTOOL COMPILATION
NAME_OTOOL	=	ft_otool
SRC_OTOOL	= 	main.c \
				otool.c \
				params.c \
				archi_64.c \
				archi_32.c \
				fat.c \
				libraries.c \
				printer.c \
				utils.c
SRC_D_OTOOL	=	src_otool/
OBJ_D_OTOOL	=	obj_otool/
OBJ_P_OTOOL	=	$(addprefix $(OBJ_D_OTOOL), $(SRC_OTOOL:.c=.o))


#GLOBAL RULES
LFT_D		=	libft/
INC_D		=	inc/

CC			=	gcc -g
CFLAGS		=	#-Werror -Wextra -Wall
LDFLAGS		=	-lft -L$(LFT_D)

all: $(OBJ_D_NM) $(OBJ_D_OTOOL) mlft $(NAME_NM) $(NAME_OTOOL)
mlft: $(LFT_D)
	@make -C $(LFT_D)

$(NAME_NM): $(OBJ_P_NM)
	@printf "$(OVERRIDE)$(CYAN)NM | $(GREEN)⌛  source to object files...\t💯 ️ done creating object files$(WHITE)\n"
	@$(CC) $^ -o $@ $(LDFLAGS)
	@printf "$(OVERRIDE)$(CYAN)NM | $(GREEN)㊗️ executable created from object files$(WHITE)\n"

$(NAME_OTOOL): $(OBJ_P_OTOOL)
	@printf "$(OVERRIDE)$(CYAN)OTOOL | $(GREEN)⌛  source to object files...\t💯 ️ done creating object files$(WHITE)\n"
	@$(CC) $^ -o $@ $(LDFLAGS)
	@printf "$(OVERRIDE)$(CYAN)OTOOL | $(GREEN)㊗️ executable created from object files$(WHITE)\n"

$(OBJ_D_NM)%.o: $(SRC_D_NM)%.c $(INC_D)/nm.h
	@mkdir $(OBJ_D_NM) 2> /dev/null || true
	@$(CC) -c $< -o $@ $(CFLAGS) -I $(INC_D) -I $(LFT_D)$(INC_D)
	@printf "$(OVERRIDE)$(CYAN)$(PROJECT) | $(GREEN)⌛  source to object files... $(YELLOW)%*s$(WHITE)" $(CURSOR_R) "$<"

$(OBJ_D_OTOOL)%.o: $(SRC_D_OTOOL)%.c $(INC_D)/otool.h
	@mkdir $(OBJ_D_OTOOL) 2> /dev/null || true
	@$(CC) -c $< -o $@ $(CFLAGS) -I $(INC_D) -I $(LFT_D)$(INC_D)
	@printf "$(OVERRIDE)$(CYAN)$(PROJECT) | $(GREEN)⌛  source to object files... $(YELLOW)%*s$(WHITE)" $(CURSOR_R) "$<"

$(OBJ_D_NM):
	@mkdir -p $(OBJ_D_NM)
	@mkdir -p $(dir $(OBJ_P_NM))
	@echo "$(CYAN)NM | $(GREEN)🆗  object directories created$(WHITE)"

$(OBJ_D_OTOOL):
	@mkdir -p $(OBJ_D_OTOOL)
	@mkdir -p $(dir $(OBJ_P_OTOOL))
	@echo "$(CYAN)OTOOL | $(GREEN)🆗  object directories created$(WHITE)"

# cosmetic rules
WHITE		=	`echo "\033[0m"`
CYAN		=	`echo "\033[36m"`
GREEN		=	`echo "\033[32m"`
RED			=	`echo "\033[31m"`
YELLOW		=	`echo "\033[33m"`
OVERRIDE	=	`echo "\r\033[K"`
CURSOR_R	=	`echo "$$(tput cols) - 38"|bc`
PROJECT		=	"NM_OTOOL"

clean:
	@make -C $(LFT_D) clean
	@rm -rf $(OBJ_D_NM) $(OBJ_D_OTOOL)
	@echo "$(CYAN)$(PROJECT) | clean $(RED)❌  object files cleaned$(WHITE)"

fclean:
	@make -C $(LFT_D) fclean
	@rm -f $(NAME_NM) $(NAME_OTOOL)
	@echo "$(CYAN)$(PROJECT) | fclean $(RED)❌  executable ($(NAME_NM), $(NAME_OTOOL)) cleaned$(WHITE)"
	@rm -rf $(OBJ_D_NM) $(OBJ_D_OTOOL)
	@echo "$(CYAN)$(PROJECT) | clean $(RED)❌  object files cleaned$(WHITE)"

re:	fclean all
	@echo "$(CYAN)$(PROJECT) | re $(YELLOW)♻️  REBUILT$(WHITE)"	

.PHONY: all clean fclean re

-include $(OBJ_P_NM:.o=.d) $(OBJ_P_OTOOL:.o=.d)
