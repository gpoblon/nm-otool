#!/bin/bash

cmd1="./ft_nm -rnj"
cmd2="nm -rnj"

for i in $@
do
	printf "\033[33m${i}\033[0m\n"
	eval "${cmd1} ${i}" >&-
	ftres=$?
	if [ $ftres -eq 139 ]; then
		read -p "CHECK, SEGMENTATION FAULT !"
	else
		diff -wu -W 155 <(eval ${cmd1} ${i}) <(eval ${cmd2} ${i})
		if [ $? -eq 1 ]; then
			if [ $ftres -eq 0 ]; then
				read -p " . . . "
				# reset
			fi
		fi
	fi
done
